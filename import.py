#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

# This script needs the following files:
#   accounts.csv        A .csv file with the accounts to import, with fields in the following order:
#       email
#       first name
#       last name
#       title

# Imports
from getpass import getpass
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
import atexit
import csv
import sys
import time

with open('accounts.csv', newline='') as csvfile:
    # Initialize webdriver
    print('Initializing Chrome')
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(chrome_options=chrome_options)
    atexit.register(driver.quit)
    driver.implicitly_wait(10)

    # Log in
    print('Loading login page')
    tdurl = 'https://test.tiredispatcher.com'
    driver.get(tdurl)
    element = driver.find_element_by_id('Email')
    element.send_keys(input('Email: '))
    element = driver.find_element_by_id('Password')
    element.send_keys(getpass())
    element.send_keys(Keys.RETURN)

    # Exploit implicit_wait to wait for page load
    # If this fails, you probably mistyped your credentials
    try:
        element = driver.find_element_by_class_name('navigationUsers')
    except:
        print('Failed to load main page. Ensure your credentials are correct and that you have the required permissions')
        exit(100)

    # Iterate through accounts
    print('Adding users')
    acctreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in acctreader:
        regurl = tdurl + '/Users/RegisterUser'
        email = row[0]
        firstname = row[1]
        lastname = row[2]
        title = row[3]
        print('Adding user ' + firstname + ' ' + lastname + ', ' + title)
        driver.get(regurl)
        # Fill out the form
        element = driver.find_element_by_id('Email')
        element.send_keys(email)
        element = driver.find_element_by_id('FirstName')
        element.send_keys(firstname)
        element = driver.find_element_by_id('LastName')
        element.send_keys(lastname)
        element = driver.find_element_by_id('Title')
        element.send_keys(title)
        element = Select(driver.find_element_by_id('TimeZoneOffsetId'))
        element.select_by_value('Central Standard Time')
        element = Select(driver.find_element_by_id('DealerId'))
        element.select_by_value('C265A511-993D-4061-9993-F0F7A5915D5E') # Kal Tire, as of time of writing
        # Additionally, there is a request verification token
        # But since we're using an actual browser, it gets submitted anyway
        # Submit
        #driver.save_screenshot(title + '-' + lastname + '-' + firstname + '.png')
        element = driver.find_element_by_id('Email')
        element.send_keys(Keys.RETURN)
