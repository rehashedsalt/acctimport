# Account Import Tool

## What is this?

It imports accounts into Tiredispatcher through browser automation.

## Where do the accounts come from?

Accounts are imported from a file in PWD called `accounts.csv`. Currently, it assumes each row contains information formatted as follows:

    Email address,First name,Last name,Position

With minimal alteration, the script is capable of parsing differently-formatted tables and applying additional logic.

The dealer association is currently hardcoded to Kal Tire through GUID. This can easily be changed.

## Invocation

    $ ./import.py

Or if your Python 3 interpreter is located outside of `/usr/bin/python3`:

    $ python3 import.py

## Dependencies

Dependencies are noted in `requirements.txt`. To install them, run `pip install -r requirements.txt`.
